import Navbar from "../../components/Navbar.tsx";

describe("Navbar", () => {
  it("should render the navbar header element", () => {
    cy.mount(<Navbar />);
    cy.get("header").should("exist");
  });

  it("should have the correct position and z-index", () => {
    cy.mount(<Navbar />);
    cy.get("header").should("have.css", "position", "static");
    cy.get("header").should("have.css", "z-index", "auto");
  });

  it("should contain a navigation link to the home page", () => {
    cy.mount(<Navbar />);
    cy.get('a[href="/"]').should("exist");
  });

  it("should display the logo image with the correct src and alt attributes", () => {
    cy.mount(<Navbar />);
    cy.get('img[src="/logo.svg"]').should("exist");
    cy.get('img[src="/logo.svg"]').should("have.attr", "alt", "Car Hub Logo");
  });
});
