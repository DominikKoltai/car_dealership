import Hero from "../../components/Hero.tsx";

describe("Hero", () => {
  it("should render the Hero component with the expected structure", () => {
    cy.mount(<Hero />);
    cy.get(".hero").should("exist");
    cy.get(".hero__title").should("exist");
    cy.get(".hero__subtitle").should("exist");
    cy.get(".hero__image-container").should("exist");
    cy.get(".hero__image").should("exist");
  });

  it("should display the correct hero title", () => {
    cy.mount(<Hero />);
    cy.get(".hero__title").should(
      "contain",
      "Find, book, or rent a car -- quickly and easily!"
    );
  });

  it("should display the correct hero subtitle", () => {
    cy.mount(<Hero />);
    cy.get(".hero__subtitle").should(
      "contain",
      "Streamline your car rental experience with our effortless booking process."
    );
  });
});
